package net.pladema.monitors.twitter.dto;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="data",name="visitasdeusuarios")
public class VisitasDeUsuarios {
	
	@Id
	@Column long user_id;
	@Column int semana;
	@Column int dia;
	@Column int franja;
	@Column int comuna;
	@Column int visitocomuna;

	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	public int getSemana() {
		return semana;
	}
	public void setSemana(int semana) {
		this.semana = semana;
	}
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	public int getFranja() {
		return franja;
	}
	public void setFranja(int franja) {
		this.franja = franja;
	}
	public int getComuna() {
		return comuna;
	}
	public void setComuna(int comuna) {
		this.comuna = comuna;
	}
	public int getVisitocomuna() {
		return visitocomuna;
	}
	public void setVisitocomuna(int visitocomuna) {
		this.visitocomuna = visitocomuna;
	}
	
}

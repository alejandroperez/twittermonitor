package net.pladema.monitors.twitter.dto;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="data")
public class Tweet {

	@Id
	@Column
	private Long id;	
	@Column
	private Calendar timestamp;	
	@Column
	private Float lat;	
	@Column
	private Float lon;
	@Column
	private Float accuracy;
	@Column(name="user_id")
	private String userId;
	@Column(name="lat_profile")
	private Float latPerfil;
	@Column(name="lon_profile")
	private Float lonPerfil;
	@Column(name="comuna")
	private Integer comuna;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Calendar getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Calendar timestamp) {
		this.timestamp = timestamp;
	}
	public Float getLat() {
		return lat;
	}
	public void setLat(Float lat) {
		this.lat = lat;
	}
	public Float getLon() {
		return lon;
	}
	public void setLon(Float lon) {
		this.lon = lon;
	}
	public Float getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(Float accuracy) {
		this.accuracy = accuracy;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Float getLatPerfil() {
		return latPerfil;
	}
	public void setLatPerfil(Float latPerfil) {
		this.latPerfil = latPerfil;
	}
	public Float getLonPerfil() {
		return lonPerfil;
	}
	public void setLonPerfil(Float lonPerfil) {
		this.lonPerfil = lonPerfil;
	}
	public Integer getComuna() {
		return comuna;
	}
	public void setComuna(Integer comuna) {
		this.comuna = comuna;
	}
	
	@Override
	public boolean equals(Object o){
		Tweet t = (Tweet)o;
		return this.id.equals(t.getId());
	}
	
	
}

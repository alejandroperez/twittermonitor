package net.pladema.monitors.twitter.dto;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(schema="data")
public class OrigenDestino {
	
	
	
	@Column
	private long valor;
	
	@Transient
	private int destino;//trabajo	
	@Id
	@Column
	private int origen;//hogar
	
	public int getDestino() {
		return destino;
	}
	public void setDestino(int destino) {
		this.destino = destino;
	}
	
	public int getOrigen() {
		return origen;
	}
	public void setOrigen(int origen) {
		this.origen = origen;
	}
	
	public void setValor(long val){
		this.valor = val;
	}
	public long getValor() {
		return valor;
	}
	public String getValorAsString(){
		return Long.toString(valor);
	}
	
}

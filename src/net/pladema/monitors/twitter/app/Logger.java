package net.pladema.monitors.twitter.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	
	private static String logDir = "./logs/";
	
	public static void Log(String msg){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		File dir = new File(logDir);		
		try{
			if(!dir.exists()){
				dir.mkdir();
			}
			String logFile = sdf.format(date)+".log";
			
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(new File(logDir+logFile),true)));
			out.println(date.toString()+": "+msg);					
			out.close();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	
	public static void main(String[] args){
		Logger.Log("Test");
	}
}

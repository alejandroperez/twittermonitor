package net.pladema.monitors.twitter.app;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import twitter4j.Status;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

public class Util {
	
	/**
	 * Convierte a JSON una geometr�a
	 * @param sourceGeometry
	 * @return JSON String
	 */
	public static String toJSONString(Geometry sourceGeometry) {
		List<String> aux_coordinates = new ArrayList<String>();					
		for(Coordinate coord : sourceGeometry.getCoordinates()){
		 aux_coordinates.add("["+coord.y+","+coord.x+"]");
		}		
		String output = StringUtils.join(aux_coordinates.toArray(),',');//Uno las coordenadas separadas por coma
		return  output;
	}
	
	//No se usa porque no se esta registrando el mensaje de cada tweet
	/**
	 * Devuelve el mensaje de un estado de Twitter, con un limite de caracteres limit 
	 * @param s Twitter status
	 * @param limit limite de caracteres del mensaje
	 * @return
	 */
	public String getStatusText(Status s, int limit){		
		if(s.getText().length() > Constants.MAX_TEXT_LENGTH)
			limit = Constants.MAX_TEXT_LENGTH;
		else{
			limit = s.getText().length()-1;
		}
		return s.getText().substring(0, limit);
	}
}

package net.pladema.monitors.twitter.app;

public class Constants {
	
	//Twitter
	public static int MAX_TEXT_LENGTH = 20;
	
	//ShpFiles	
	public static String COMUNAS_SHP = "resources/comunas_caba/comunas_censo_2010.shp";
	public static String BARRIOS_SHP = "resources/barrios_caba/barrios_censo_2010.shp";
	//ShpConstants
	public static String COMUNAS_SHP_NAME_COLUMN = "COMUNAS"; 

}

package net.pladema.monitors.twitter.app;

import java.io.BufferedWriter;
import java.io.FileWriter;

import net.pladema.monitors.twitter.db.DAOFacade;
import net.pladema.monitors.twitter.integration.MatrizOD;
import net.pladema.monitors.twitter.integration.TwitterRequestScheduler;

public class MonitorApp {
	
	private TwitterRequestScheduler scheduler;
	private MatrizOD matriz;
	
	public MonitorApp(String fechaDesde, String fechaHasta){
		
		scheduler = new TwitterRequestScheduler(fechaDesde, fechaHasta);
		//scheduler = new TwitterRequestScheduler(125889223);
	}
	
	public static void main(String[] args) throws InterruptedException{
		//Init de twitter4j (monitor)
		String fechaDesde = null;
		String fechaHasta = null;
		switch(args.length){
			case 1: fechaDesde = args[0];
					break;
			case 2: fechaDesde = args[0];
					fechaHasta = args[1];
					System.out.println("Recuerde cerrar el monitor luego de compeltar la carga.");
					System.out.println("En este modo NO se continuará cargando los proximos tweets...");
					Thread.sleep(2000);
					break;
			default: break;
			
		}
		MonitorApp myApp = new MonitorApp(fechaDesde, fechaHasta);
		//MatrizOD m = new MatrizOD();
		
		//Ejemplo para consultar la comuna para un punto lat,lng
//		Point2d testPoint = new Point2d(-34.5600111, -58.4596);
//		String comuna = comunas.getComunaAsociada(testPoint.x,testPoint.y);
//		if(comuna!= null){
//			System.out.println("Punto situado en la comuna: "+comuna);
//			Places.printGeometry(comunas.getGeometry(comuna));
//		}else{
//			System.out.println("no se hay� una comuna para ese punto");
//		}

	}
}

package net.pladema.monitors.twitter.app;

//File Name SendEmail.java

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class SendEmail
{	
	
public static void main(String [] args, String _asunto, String _mensaje)
{    
			// SMTP server information
			final String host = "smtp.gmail.com";
			final int port = 487;
			final String username = "twitter.monitor.app@gmail.com";
			final String password = "plademio";
			final String cc = "alecio88@gmail.com";
	 
			// email information
			String from = "lddsoluciones@gmail.com";
			String to   = "twitter.monitor.app@gmail.com";
			
			// Step 1) get the connection by authentication via TLS/StartTLS
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  }
			);

   try{
      // Create a default MimeMessage object.
      MimeMessage message = new MimeMessage(session);

      // Set From: header field of the header.
      message.setFrom(new InternetAddress(from));

      // Set To: header field of the header.
      message.addRecipient(Message.RecipientType.TO,
                               new InternetAddress(to));

      message.addRecipients(Message.RecipientType.CC, 
              InternetAddress.parse(cc));
      
      // Set Subject: header field
      message.setSubject("Asunto: "+_asunto);

      // Now set the actual message
      message.setText("Mensaje de TwitterMonitor: "+ _mensaje);

      // Send message
      Transport.send(message);
      System.out.println("Mensaje enviado correctamente....");
   }catch (MessagingException mex) {
      mex.printStackTrace();
   }
}

}


 
package net.pladema.monitors.twitter.app;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import net.pladema.monitors.twitter.db.ConnectionPool;
import net.pladema.monitors.twitter.db.DAOFacade;
import net.pladema.monitors.twitter.dto.Tweet;

import org.apache.commons.lang3.StringUtils;

public class ComunasProcessor {
	
	public static void PrintShpAsGeoJSON(){
		
		
		Places comunas = new Places(Constants.COMUNAS_SHP);
		String[] comunas2 = comunas.getComunas();
		List<String> shps_comunas = new ArrayList<String>();
		for(String s : comunas2){	
			if(Integer.valueOf(s).equals(14) || Integer.valueOf(s).equals(3))
				shps_comunas.add(Places.printGeometry(comunas.getGeometry(s)));											
		}
		
		//Guardo archivo
		PrintWriter out = null;
		
		try {
			out = new PrintWriter("geojson.txt");
			out.print(StringUtils.join(shps_comunas,"\n"));
			out.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	public static void main(String[] args){
		
		ComunasProcessor.PrintShpAsGeoJSON();
		
		//Procesador de Comunas		
//		Places comunas = new Places(Constants.COMUNAS_SHP);
//		DAOFacade dao = new DAOFacade(ConnectionPool.getSessionFactory());
//		
//		List<Tweet> lst = null;
//		//Long current = 564042393565880320L;
//		Long current = 585762427876802301L;
//		Long interval = 1279965689078020L;
//		while (current < 587042393565880320L){
//			lst = dao.obtenerTweets(current, 
//					current + interval);
//			current = current + interval;
//				
//			System.out.println("Se analizarán "+ lst.size()+ " tweets");
//			List<Tweet> updated = new ArrayList<Tweet>();
//			for(Tweet t : lst){
//				String comunaAsociada = comunas.getComunaAsociada(t.getLat(), t.getLon());
//				if(comunaAsociada!=null){
//					t.setComuna(Integer.parseInt(comunaAsociada));
//					updated.add(t);
//				}
//			}
//			
//			dao.updateTweets(updated);
//		}
//		System.out.println("termino");
	}

}

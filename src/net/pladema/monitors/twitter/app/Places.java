package net.pladema.monitors.twitter.app;

import java.io.File;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

public class Places {
	
	//private ShpFiles comunasShp = null; 
	private Map<String, Geometry> comunas = new Hashtable<String,Geometry>();
	
	private CoordinateReferenceSystem worldCRS = null;
	
	public Places(String shapeFile){
		try {
			worldCRS = CRS.decode("EPSG:4326");
		} catch (FactoryException e) {
			e.printStackTrace();
		}
		File comunasShp = new File(shapeFile);
		loadShpFile(comunasShp); 	
	}
	
	private void loadShpFile(File file){
		try {
			
			Map<String,Object> connect = new HashMap<String,Object>();
			connect.put("url", file.toURI().toURL());
			
			DataStore dataStore = DataStoreFinder.getDataStore(connect);
			String[] typeNames = dataStore.getTypeNames();
			String typeName = typeNames[0];
			
			System.out.println("Reading content " + typeName);
			
			SimpleFeatureSource featureSource = dataStore.getFeatureSource(typeName);		  		  
			SimpleFeatureCollection collection = featureSource.getFeatures();
			SimpleFeatureIterator iterator = collection.features();

			//Transformacion de coordenadas		  			
			CoordinateReferenceSystem dataCRS = featureSource.getSchema().getCoordinateReferenceSystem(); // Para obtener CRS del ShpFile								
			boolean lenient = true; // allow for some error due to different datums
			MathTransform transform = CRS.findMathTransform(dataCRS, worldCRS, lenient);					
				  				  			
			try {
				while (iterator.hasNext()) {
					SimpleFeature feature = iterator.next();		      
					Geometry sourceGeometry = (Geometry) feature.getDefaultGeometry();
					sourceGeometry = JTS.transform(sourceGeometry, transform);
					/* IDs Comunas: Guarda la lista de IDs de comunas */
					comunas.put(feature.getAttribute(Constants.COMUNAS_SHP_NAME_COLUMN).toString(),sourceGeometry);					
								
					// JSON output
					//String result = Util.toJSONString(sourceGeometry);
					//System.out.println("["+result+"]");
			  }
			} finally {
			  iterator.close();
			}
			
			} catch (Exception e) {
				e.printStackTrace();
			}
	}	
	
	public String[] getComunas(){
		return comunas.keySet().toArray(new String[]{});
	}
	
	public Geometry getGeometry(String idComuna){
		return comunas.get(idComuna);
	}
	
	public static String printGeometry(Geometry geometry){
		String geom = "["+Util.toJSONString(geometry)+"]";
		System.out.println(geom);
		return geom;
	}
	
	public String getComunaAsociada(double lat, double lon){
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();		    
	    Coordinate coord = new Coordinate(lat, lon);
	    Point location = geometryFactory.createPoint(coord);
		for(String key : getComunas()){
			Geometry geometry = comunas.get(key);
			if(geometry.contains(location)){
				return key;
			}
		}
		return null;
	}
	
	
	
}

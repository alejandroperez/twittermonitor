package net.pladema.monitors.twitter.integration.matlab;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import MatlabJava.Java2Matlab;

import com.mathworks.toolbox.javabuilder.MWCellArray;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

import net.pladema.monitors.twitter.db.ConnectionPool;
import net.pladema.monitors.twitter.db.DAOFacade;
import net.pladema.monitors.twitter.dto.OrigenDestino;

public class Matrix2Matlab {

	private DAOFacade dao = new DAOFacade(ConnectionPool.getSessionFactory());
	private int franja;
	private int visitasMinimas;
	
	public List<List<OrigenDestino>> odMatrix;
	private String filename ="odmatrix.mat";
	
	public Matrix2Matlab(){
		franja = 2;
		visitasMinimas = 2;
		odMatrix = new ArrayList<List<OrigenDestino>>();		
		RetrieveValues();
	}

	private void RetrieveValues() {
		for(int comuna = 1; comuna < 16; comuna++){
			List<OrigenDestino> fila = dao.obtenerFilaOrigenDestino(comuna,franja,visitasMinimas);			
			odMatrix.add(fila);
		}		
	}
	
	public void printMatrix(){
		
		List<OrigenDestino> flat = 
			    odMatrix.stream()
			        .flatMap(l -> l.stream())
			        .collect(Collectors.toList());
		
		long[][] mat = new long[15][15];		
		
		for(int f=1;f<16;f++){
			for(int c=1;c<16;c++){
				final int fila = f;
				final int columna = c;
				List<OrigenDestino> result = flat.stream().filter(
						celda -> celda.getOrigen() == columna
						&& celda.getDestino() == fila
						).collect(Collectors.toList());
				if(result!= null && result.size() > 0){
					OrigenDestino celda = result.get(0);
					System.out.println(celda.getDestino()+","+celda.getOrigen()+":"+celda.getValor());					
					mat[fila-1][columna-1]=celda.getValor();
				}
				else{
					System.out.println(f+","+c+":"+0);
					mat[fila-1][columna-1]=0;
				}
			}
		}
		
		guardar(mat, filename );
			
	}
	
	public void guardar(long[][] mat, String filename){
		MWCellArray estructura = new MWCellArray(new int[]{mat.length,mat.length});
		
		for(int fil=1;fil<=mat.length;fil++){
			for(int col=1;col<=mat.length;col++){
				estructura.set(new int[]{fil,col}, new MWNumericArray(new Double(mat[fil-1][col-1])));
			}
		}				
		
		try {
			Java2Matlab j2m = new Java2Matlab();
			j2m.guardar(estructura, filename);
		} catch (MWException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Escribio: "+filename);
	}
	
	public static void main(String[] args){
		Matrix2Matlab matlab = new Matrix2Matlab();
		matlab.printMatrix();
	}
			
	
	
}

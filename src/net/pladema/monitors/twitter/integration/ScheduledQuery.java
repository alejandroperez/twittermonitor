package net.pladema.monitors.twitter.integration;

import java.util.List;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.TwitterException;
import net.pladema.monitors.twitter.app.Logger;

public class ScheduledQuery{
	
	private Query query;
	private Long lastIndex;
	
	public ScheduledQuery(Query query){
		this.query = query;
		this.lastIndex = 0L;
	}
	
	public Query getNextQueryStep(){
		
		if(lastIndex>0)
		{
			query.setMaxId(lastIndex);
		}
		return query;
		
	}
	
	public boolean hasMoreSteps(){
		return (lastIndex == -1L) ? false : true; // Si es -1 ya termin� de hacer consultas, los proximos resultados ser�n vacios entonces no hay mas pasos.
	}
	
	public QueryResult run() throws TwitterException{
		Query query = getNextQueryStep();
		if(query == null) return null;
		
		QueryResult result = TwitterRequestScheduler.twitter.search(query);			
		List<Status> tweets = result.getTweets();
				
		//SI NO TRAE MAS O LO QUE TRAE ES LO QUE YA TRAJO
		if(tweets.size() == 0 || (tweets.size() == 1 && tweets.get(tweets.size()-1).getId() == lastIndex)){			
			lastIndex=-1L;			
		}else{
			Status last = tweets.get(tweets.size()-1);		
			if(last!= null){
				lastIndex = last.getId();
			}
		}
		
		//Logging
		Logger.Log("[run] "+ "lastIndex: "+lastIndex+ ", tweetsSize:"+tweets.size());

		
		return result;
	}
	
	public Query getQuery(){
		return this.query;
	}
	
	
	
}

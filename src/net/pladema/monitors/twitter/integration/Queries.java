package net.pladema.monitors.twitter.integration;

import twitter4j.GeoLocation;
import twitter4j.Query;

public class Queries {
	
	public static ScheduledQuery getLatestGeolocatedTweets(int count){		
		Query query = new Query("");		
	    query.setGeoCode(new GeoLocation(-37.3178101, -59.1503906), 10, Query.KILOMETERS);	    	   
	    //query.setMaxId(new Long("546349535401115649"));
	    //Paging page = new Paging(1,20);
	    query.setCount(count);
	    
	    ScheduledQuery squery = new ScheduledQuery(query);
	    return squery;
	}
	
	public static ScheduledQuery getGeolocatedTweets(int count, String dateFrom, String dateTo){		
		Query query = new Query("");
		//TANDIL
	    //query.setGeoCode(new GeoLocation(-37.3178101, -59.1503906), 10, Query.KILOMETERS);
		//BUENOS AIRES
		query.setGeoCode(new GeoLocation(-34.6120185, -58.4338760), 11, Query.KILOMETERS);		
	    query.setSince(dateFrom);
	    query.setUntil(dateTo);	    
	    //query.setMaxId(new Long("546349535401115649"));
	    query.setCount(count);
	    	    
	    ScheduledQuery squery = new ScheduledQuery(query);
	    return squery;
	}
}

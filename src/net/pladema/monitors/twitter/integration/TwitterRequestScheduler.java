package net.pladema.monitors.twitter.integration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import net.pladema.monitors.twitter.app.SendEmail;
import net.pladema.monitors.twitter.db.ConnectionPool;
import net.pladema.monitors.twitter.db.DAOFacade;
import net.pladema.monitors.twitter.dto.Tweet;
import twitter4j.Logger;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class TwitterRequestScheduler {
	
	private static final int ERR_LIMIT_EXCEEDED = 429;
	private static Logger logger = Logger.getLogger(TwitterException.class);
	
	private List<ScheduledQuery> queue = new ArrayList<ScheduledQuery>();	
	public static Twitter twitter = TwitterFactory.getSingleton();
	private RateLimitStatus lastRateLimitStatus;	
	DAOFacade dao = new DAOFacade(ConnectionPool.getSessionFactory());
	private Status ultimo;
	
	public TwitterRequestScheduler(String desde, String hasta){
		//dao.clearTweets();
		try {			
			initTwitterMonitor(desde, hasta);
		} catch (TwitterException e) {
			System.out.println("Se agotaron los recursos");
			e.printStackTrace();
		}
	}
	
	/***
	 * Trae el timeline de un usuario por id
	 * @param user_id
	 */
	public TwitterRequestScheduler(long user_id) {
		try {
			String filename = new Date().getTime() + ".csv";
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filename), true));
			ResponseList<Status> userTimeline = twitter
					.getUserTimeline(user_id);
			WriteResult(writer, userTimeline);
						
		}
		catch (TwitterException | IOException e) {
			e.printStackTrace();
			//enviar email con errores
			SendEmail.main(null, "Problema en TwitterRequestScheduler", "Se produjo el mensaje: "+ e.getMessage());			
		}
	}
	
	/**
	 * Dice si esta dentro de los ultimos 7 dias
	 * @param fechaDesde
	 * @return
	 */
	private boolean validDate(Date fechaDesde){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -8);
		return fechaDesde.after(cal.getTime()) && fechaDesde.before(new Date());
		
	}
	private void initTwitterMonitor(String desde, String hasta) throws TwitterException {
		
		updateRateLimitStatus();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		while(true){
			//Busco el dia actual
			Date today = new Date();			
			//Calculo dia anterior
			Calendar cal = Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, -1);
			Date yesterday = null;
			if(desde!=null){
				Date fechaDesde = null;
				try {
					fechaDesde = sdf.parse(desde);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					System.err.println("Fecha desde inválido");
					return;
				}				
				if(fechaDesde != null && validDate(fechaDesde)){
					yesterday = fechaDesde;
					fechaDesde = null; //Importante! Saco de circulación el parametro para los siguientes dias.
				}
				else{
					System.err.println("La fecha no está dentro de los últimos 7 días");
					return;
				}
			}else{
				yesterday = cal.getTime();
			}			
			String ayer = sdf.format(yesterday);
			//Fecha Hasta
			String hoy = null;
			if(hasta!= null){
				try {
					sdf.parse(hasta);
				} catch (ParseException e) {
					System.err.println("Fecha hasta inválido");
					return;
				}
				hoy = hasta;
			}
			else{
				hoy = sdf.format(today);
			}
			logger.info("Buscando tweets desde "+ayer+" hasta "+hoy);			
			ScheduledQuery query = Queries.getGeolocatedTweets(100, ayer, hoy);
			int count = runQuery(query, new Date().getTime() + ".csv");
			SendEmail.main(null, "Estado Correcto", "Se completó un dia de carga con: "+count+" cargas");
			// Me duermo por un dia
			try {
				TimeUnit.DAYS.sleep(1L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		
	}

	private void updateRateLimitStatus()  {
		Map<String, RateLimitStatus> rateLimitStatus = null;
		try {
			rateLimitStatus = twitter
					.getRateLimitStatus("search");
			lastRateLimitStatus = rateLimitStatus.get("/search/tweets");			
		} catch (TwitterException e) {
			if(e.getStatusCode()==ERR_LIMIT_EXCEEDED){
				dormir();
				updateRateLimitStatus();
			}
			else{
				System.err.println("ERROR "+e.getStatusCode()+"-"+e.getMessage());
			}
		}
			
	}

	public int runQuery(ScheduledQuery query,String filename){
		int count=0;
		while(query.hasMoreSteps())
		{
			System.out.println("Comienza query");
			if(hasResources()){
				System.out.println("Quedan :"+lastRateLimitStatus.getRemaining()+ " pedidos");
				Writer writer = null;
				
				try{
					writer = new BufferedWriter(new FileWriter(new File(filename), true));
					QueryResult result = query.run();
					WriteResult(writer, result.getTweets());
					count += result.getTweets().size();
					System.out.println("Cargo:" +result.getTweets().size());
					//Logging
					net.pladema.monitors.twitter.app.Logger.Log("[runQuery] "+ "hasMoreSteps: "+query.hasMoreSteps()+ ", resultTweetsSize:"+result.getTweets().size());

				} catch(TwitterException ex){				
					//Por algun motivo tenía recursos pero falló
					logger.error(ex.getMessage());
					System.err.println(ex.getMessage());
					SendEmail.main(null, "Problema en runQuery", "Se produjo el mensaje: "+ ex.getMessage());
					updateRateLimitStatus(); //Lo pongo a dormir por las dudas que se resuelva el problema durmiendo
				} catch (IOException ex) {
					System.out.println("No se pudo abrir el archivo");
					System.err.println(ex.getMessage());
					//enviar email con errores
					SendEmail.main(null, "Problema en runQuery", "No se pudo abrir el archivo. Se produjo el mensaje: "+ ex.getMessage());
				} finally {
					try {
						writer.close();		
						//SendEmail.main(null, "Estado Correcto", "La carga diaria fue correcta");
					} catch (Exception ex) {
						System.out.println("Fallo al cerrar el archivo");
						SendEmail.main(null, "Problema en runQuery", "Fallo al cerrar el archivo. Se produjo el mensaje: "+ ex.getMessage());
					}
				}
				try {
					System.out.println("se durmio");
					Thread.sleep(1000);
					System.out.println("se desperto");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}				
			
		}
		return count;
	}
	
	private void dormir(){
		try {
			int ms = lastRateLimitStatus==null ? 60000 : (lastRateLimitStatus.getSecondsUntilReset() + 1) * 1000;
			System.out.println("Se quedo sin recursos, hora:"
					+ new Date() + " Duerme por "+ms);			
			
			Thread.sleep(ms);
			
			updateRateLimitStatus();//Actualizamos los limites
			System.out.println("Se desperto a las " + new Date()
					+ ", con límite:"
					+ lastRateLimitStatus.getRemaining());
		} catch (InterruptedException e) {
			e.printStackTrace();
			SendEmail.main(null, "Problema en dormir", "Se produjo el mensaje: "+ e.getMessage());
		}
	}

	private void WriteResult(Writer writer, List<Status> tweets)
			throws IOException {

		if (tweets.size() == 0)
			return;

		System.out.println("tweet_id,timestamp,lat,lon,user_id");
		List<Tweet> tweetList = new ArrayList<Tweet>();

		if (ultimo != null && ultimo.getId() == tweets.get(0).getId()) {
			tweets.remove(0);
		}

		if (tweets.size() == 0)
			return;
		ultimo = tweets.get(tweets.size() - 1);
		for (Status status : tweets) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			String date = sdf.format(status.getCreatedAt());
			if (status.getGeoLocation() != null) {
				String formattedLine = status.getId() + "," + date + ","
						+ status.getGeoLocation().getLatitude() + ","
						+ status.getGeoLocation().getLongitude() + ","
						+ status.getUser().getId();
				Tweet nuevotweet = new Tweet();
				nuevotweet.setId(status.getId());
				nuevotweet
						.setLat((float) status.getGeoLocation().getLatitude());
				nuevotweet.setLon((float) status.getGeoLocation()
						.getLongitude());
				nuevotweet.setUserId(String.valueOf(status.getUser().getId()));
				Calendar cal = new GregorianCalendar();
				cal.setTime(status.getCreatedAt());
				nuevotweet.setTimestamp(cal);

				tweetList.add(nuevotweet);

				// System.out.println(formattedLine);
				writer.append(formattedLine + "\n");
			}
		}
		dao.addTweets(tweetList);
		writer.append("\n");
	}
	
	/***
	 * Verifica y espera hasta tener recursos disponibles.  
	 * @return
	 */
	private boolean hasResources() {		
		updateRateLimitStatus();		
		if(lastRateLimitStatus==null) return false;
		return (lastRateLimitStatus.getRemaining() > 0) ? true : false;
	}
} 


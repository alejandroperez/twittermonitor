package net.pladema.monitors.twitter.integration;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.border.Border;

import org.apache.commons.lang3.StringUtils;


import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;

import net.pladema.monitors.twitter.db.ConnectionPool;
import net.pladema.monitors.twitter.db.DAOFacade;
import net.pladema.monitors.twitter.dto.OrigenDestino;
import net.pladema.monitors.twitter.dto.VisitasDeUsuarios;


public class MatrizOD {

	private DAOFacade dao = new DAOFacade(ConnectionPool.getSessionFactory());
	public MatrizOD(){
		try {
			CreateOD();
		} catch (IOException e) {			
			e.printStackTrace();
		}								
	}	
	
	public void crearXLS() throws IOException{
		// create a new file
				FileOutputStream out = new FileOutputStream("workbook.xls");
				// create a new workbook
				Workbook wb = new HSSFWorkbook();
				// create a new sheet
				Sheet s = wb.createSheet();
				s.setColumnWidth(1, 11*256);
				// create a cell styles
				CellStyle csHeader = wb.createCellStyle();
				crearStyleHeader(csHeader,wb);
				CellStyle csValores = wb.createCellStyle();
				crearStyleValores(csValores, wb);
				CellStyle csBorderRight = wb.createCellStyle();
				csBorderRight.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
				crearStyleValores(csBorderRight, wb);
				CellStyle csBorderBottom = wb.createCellStyle();
				csBorderBottom.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
				crearStyleValores(csBorderBottom, wb);
				CellStyle csBorderRightBottom = wb.createCellStyle();
				csBorderRightBottom.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
				csBorderRightBottom.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
				crearStyleValores(csBorderRightBottom, wb);

				//set the sheet name
				wb.setSheetName(0, "MatrizOD");
											
				// declare a cell object reference				
				short rownum = 0;
				
				crearMatrizValores(s, wb, rownum, csHeader, csValores, csBorderRightBottom, csBorderRight, csBorderBottom);
				crearMatrizPorcentajes(s, wb, rownum, (short) (rownum + 20), csHeader, csValores, csBorderRightBottom, csBorderRight, csBorderBottom);
				wb.write(out);
				out.close();				
	}
	
	private void crearMatrizValores(Sheet s, Workbook wb, short rownum, CellStyle csHeader, CellStyle csValores, CellStyle csBorderRightBottom, CellStyle csBorderRight, CellStyle csBorderBottom){
		
		crearEncabezados(s,wb,rownum,csHeader);
		Cell c = null;
		int Franja = 2;
		int VisitasMinimas=2;		
		for (short row = 2; row <= rownum+16; row++)
		{
			List<OrigenDestino> fila = dao.obtenerFilaOrigenDestino(row-1,Franja,VisitasMinimas);
			//Creo las filas cuando defino los encabezados 					
		    for (short cellnum = (short) 2; cellnum <= 16; cellnum ++)
		    {
		    	//Las columnas se crearon cuando creo los encabezados
		        c = s.getRow(row).createCell(cellnum);				        				        				        
		        c.setCellValue(obtenerCantidadOrigenDeFila(fila,(short) (cellnum-1)));				      		
		        c.setCellStyle(csValores);
		        if (row == 16 && cellnum == 16)
		        	c.setCellStyle(csBorderRightBottom);
		        else{
			        if (cellnum==16)
			        	c.setCellStyle(csBorderRight);			        				        
			        if (row==rownum+16)				    
			        	c.setCellStyle(csBorderBottom);
		        }
		    }
		    c = s.getRow(row).createCell(17);
		    CellReference celTotalInicio = new CellReference(row,2);
		    CellReference celTotalFin = new CellReference(row,16);
		    String sumaTotal = "SUM($"+celTotalInicio.formatAsString().replace("$", "") +":$"+celTotalFin.formatAsString().replace("$", "")+")";
		    //c.setCellValue(getSumaFila(row,s,2));
		    c.setCellFormula(sumaTotal);
		}
	}
	
	private void crearMatrizPorcentajes(Sheet s, Workbook wb, short rowNumDatos, short rowNumActual, CellStyle csHeader, CellStyle csValores, CellStyle csBorderRightBottom, CellStyle csBorderRight, CellStyle csBorderBottom){
		
		crearEncabezados(s,wb,rowNumActual,csHeader);
		Cell c = null;
				
		for (short row = 2; row <= 16; row++)
		{		
			//Creo las filas cuando defino los encabezados 					
		    for (short cellnum = (short) 2; cellnum <= 16; cellnum ++)
		    {		
		    	//Las columnas se crearon cuando creo los encabezados
		        c = s.getRow(rowNumActual+row).createCell(cellnum);				        				        				        
		        c.setCellFormula(obtenerPorcentajeCelda((short)(rowNumDatos+row),cellnum,s));				      		
		        c.setCellStyle(csValores);
		        if (row == 16 && cellnum == 16)
		        	c.setCellStyle(csBorderRightBottom);
		        else{
			        if (cellnum==16)
			        	c.setCellStyle(csBorderRight);
			        if (row==16)				    
			        	c.setCellStyle(csBorderBottom);
		        }
		    }
		    c = s.getRow(rowNumActual+row).createCell(17);
		    CellReference celTotalInicio = new CellReference(rowNumActual+row,2);
		    CellReference celTotalFin = new CellReference(rowNumActual+row,16);
		    String sumaTotal = "SUM($"+celTotalInicio.formatAsString().replace("$", "") +":$"+celTotalFin.formatAsString().replace("$", "")+")";
		    //c.setCellValue(getSumaFila(row,s,2));
		    c.setCellFormula(sumaTotal);
		}
	}
	
	private String obtenerPorcentajeCelda(short row, short cell,Sheet s) {						
		CellReference cel = new CellReference(row, cell);
		CellReference celTotal = new CellReference(row,17);
		//double valorCelda = s.getRow(row).getCell(cell).getNumericCellValue();
		String porcentaje = "$"+cel.formatAsString().replace("$", "") +"*100/$"+celTotal.formatAsString().replace("$", "") ;  
				//valorCelda * 100 / sumaFila;
		//String porcentaje = "="+cel.formatAsString() +"*100/"+celTotal.formatAsString();
		//System.out.println(porcentaje);
		return porcentaje;
	}

	private double getSumaFila(short row, Sheet s, int initCol) {
		double valorParcial = 0;
		for (int i = initCol;i<initCol+15;i++){			
			valorParcial = valorParcial + s.getRow(row).getCell(i).getNumericCellValue();
		}
		return valorParcial;
	}

	private void crearStyleHeader(CellStyle cs,Workbook wb) {
		// create a fonts objects
		Font f = wb.createFont();
		//set font 1 to 12 point type
		f.setFontHeightInPoints((short) 12);

		f.setColor(HSSFColor.BLACK.index);
		// make it bold
		//arial is the default font
		f.setBoldweight(Font.BOLDWEIGHT_BOLD);		
		//set cell stlye
		cs.setFont(f);	
		cs.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
				
		cs.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		cs.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		cs.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		cs.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		cs.setAlignment(CellStyle.ALIGN_CENTER);
		cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);		
	}

	private void crearStyleValores(CellStyle cs,Workbook wb){		
		Font f = wb.createFont();
		//set font 1 to 12 point type
		f.setFontHeightInPoints((short) 12);
		f.setColor(HSSFColor.BLACK.index);
		cs.setFont(f);	
		cs.setAlignment(CellStyle.ALIGN_CENTER);
		//DataFormat format = wb.createDataFormat();		
		//cs.setDataFormat(format.getFormat("00"));				
	}
	private void crearEncabezados(Sheet s, Workbook wb, short rownum, CellStyle cs) {	
		// declare a row object reference
		Row r,r2 = null;
		// declare a cell object reference
		Cell c = null;				
		r = s.createRow(rownum); //fila0
		r2 = s.createRow(rownum+1);//fila1
        for (int i = 1;i<=16;i++){ //celdas de 0 a 16
        	c = r.createCell(i);	
        	c.setCellStyle(cs);
        	if (i==1)
        		c.setCellValue("Origen");
        	c = r2.createCell(i);		
        	c.setCellStyle(cs);
        	if (i>1)
        		c.setCellValue(i-1);
        }        
        r2.getCell(1).setCellValue("Comuna");
        c = r2.createCell(17);
        c.setCellStyle(cs);
        c.setCellValue("Total");                      
       // cs.setRotation((short)90);
        for (int j = rownum+2;j<=rownum+16;j++){ //fila de 2 a 16 
        	r = s.createRow(j);
        	c = r.createCell(0);		//celda 0
        	c.setCellStyle(cs);
        	if (j==rownum+2)
        		c.setCellValue("Destino");        	
        	c = r.createCell(1);		//celda 1
        	c.setCellStyle(cs);        	
        	c.setCellValue(j-1-rownum);
        }
        s.addMergedRegion(new CellRangeAddress(rownum, rownum, 1, 16)) ;
        s.addMergedRegion(new CellRangeAddress(rownum+2, rownum+16, 0, 0)) ;
        
        
	}

	public void CreateOD() throws IOException{
		
		crearXLS();
		
/*
		BufferedWriter br = new BufferedWriter(new FileWriter("matrizOD.csv"));
		StringBuilder sb = new StringBuilder();
		
		int Franja = 2;
		int VisitasMinimas=2;		
		for (short ComunaDestino = 1; ComunaDestino<=15 ; ComunaDestino++){
			List<OrigenDestino> fila = dao.obtenerFilaOrigenDestino(ComunaDestino,Franja,VisitasMinimas);
			List<String> auxFila=new ArrayList<String>();
			for (short i = 1; i<=15 ; i++){
				auxFila.add(obtenerCantidadOrigenDeFila(fila,i));				
			}
			sb.append(StringUtils.join(auxFila, ';'));
			br.write(sb.toString());
			br.write('\n');
			sb = new StringBuilder();
		}
		br.close();
		*/
	}

	private long obtenerCantidadOrigenDeFila(List<OrigenDestino> fila, short i) {
		for(OrigenDestino od : fila){
			if(od.getOrigen() == i){
				return od.getValor();
			}
		}
		return 0;
	}
}

package net.pladema.monitors.twitter.db;

import java.util.ArrayList;
import java.util.List;

import net.pladema.monitors.twitter.dto.OrigenDestino;
import net.pladema.monitors.twitter.dto.Tweet;
import net.pladema.monitors.twitter.dto.VisitasDeUsuarios;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class DAOFacade {
	
	private SessionFactory sessionFactory;
	
	public DAOFacade(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	public void addTweets(List<Tweet> tweetList){
		if(tweetList.size() == 0) return;
		try{
			Session session = this.sessionFactory.openSession();		 
		    Transaction transaction = session.beginTransaction();	    	    
		    for(Tweet t : tweetList){
		    	session.save(t);
		    }	    	  
		    transaction.commit();	    
		    session.close();
		    session.disconnect();
		}
		catch(HibernateException ex){			
			long from = tweetList.get(tweetList.size()-1).getId();
			long to = tweetList.get(0).getId();
			List<Tweet> existentes = this.obtenerTweets(from, to);
			List<Tweet> restantes = filter(existentes,tweetList);
			System.out.println("Había "+existentes.size()+" tweets ya agregados, se agregaron:"+restantes.size());
			addTweets(restantes);
			
		}
			    		
	}
	
	private List<Tweet> filter(List<Tweet> existentes, List<Tweet> tweetList) {
		List<Tweet> restantes = new ArrayList<Tweet>();
		for(Tweet t :tweetList){
			if(!existentes.contains(t)){
				restantes.add(t);
			}
		}
		return restantes;
	}

	public void clearTweets(){
		Session session = this.sessionFactory.openSession();
		 
	    Transaction transaction = session.beginTransaction();	    	    
	    Query q = session.createQuery("delete Tweet");
	    q.executeUpdate();	    
	    transaction.commit();	    
	    session.close();
	    session.disconnect();
	}
	
	public List<Tweet> obtenerTweets(long idFrom, long idTo){
		Session session = this.sessionFactory.openSession();
		 
	    Transaction transaction = session.beginTransaction();	    	    
	    Query q = session.createQuery("From Tweet WHERE id >= ? AND id <= ?");
	    q.setLong(0, idFrom);
	    q.setLong(1, idTo);
	    List<Tweet> list = q.list();  
	    transaction.commit();
	    session.close();
	    if(session.isConnected())
	    	session.disconnect();
	    return list;
		
	}
	
	public void updateTweets(List<Tweet> listaTweets){
		Session session = this.sessionFactory.openSession();
		 
	    Transaction transaction = session.beginTransaction();	    	    	    
	    for(Tweet t : listaTweets){
	    	session.update(t);
	    }	    
	    transaction.commit();
	    session.close();
	    session.disconnect();
	}
	

	public List<OrigenDestino> obtenerFilaOrigenDestino(int ComunaDestino, int _franja, int _VisitasMinimas){
		// _VisitasMinimas filtra el numero de visitas que se exije por comuna para considerarla visitada
		Session session = this.sessionFactory.openSession();
		
	    Transaction transaction = session.beginTransaction();	    	    
	    String consulta = 
	    		"WITH usuariosvalidos AS("+	    		
	    			"SELECT user_id , count(cvisitadas) "+
	    			"FROM ("+
	    				"SELECT user_id, franja, sum(visitocomuna) AS cvisitadas "+
	    				"FROM twitter.visitasdeusuarios v "+
	    				"group by user_id, franja"+
	    			") q "+
	    			"group by user_id "+
	    			"having COUNT(cvisitadas) = "+ _VisitasMinimas +		    			
	    		"),"+
	    		"visitasvalidas AS("+
	    			"SELECT * "+
	    			"FROM twitter.visitasdeusuarios "+
	    			"WHERE user_id IN( SELECT user_id FROM usuariosvalidos) "+
	    			"AND semana > 8 "+
	    		"),nocturnos AS ("+
	    			"SELECT user_id, comuna, cant, "+ 
	    				"ROW_NUMBER() OVER(PARTITION BY q.user_id "+ 
	    							 "ORDER BY q.cant DESC) AS rk "+
	    			"FROM ("+ 	    			
	    				"SELECT user_id,  comuna, sum(visitocomuna) as cant "+
	    				"FROM visitasvalidas "+
	    				"WHERE franja = 4 "+		
	    				"GROUP BY user_id, comuna"+				
	    			") q "+
	    			"WHERE cant IS NOT NULL "+
	    			"order by user_id, cant desc"+
	    		"),"+
	    		"hogar AS( "+
	    		"SELECT user_id, comuna "+
	    		"FROM nocturnos "+
	    		"WHERE rk = 1"+
	    		")"+

	    		"SELECT comuna as origen, COUNT(h.user_id) as valor "+
	    		"FROM ("+	    			
	    			"SELECT DISTINCT(user_id) "+
	    			"FROM visitasvalidas " +
	    			"WHERE franja = "+_franja+ " AND "+ 	    			
	    			"comuna = "+ComunaDestino +" AND visitocomuna IS NOT NULL"+
	    		") q INNER JOIN hogar h ON ( q.user_id = h.user_id) "+
	    		"group by comuna "+
	    		"order by comuna asc ";

    	Query q = session.createSQLQuery(consulta).addEntity(OrigenDestino.class);
	    List<OrigenDestino> list = q.list();
	    for(OrigenDestino od : list){
	    	od.setDestino(ComunaDestino);
	    }
	    
	    
	    transaction.commit();
	    session.close();
	    return list;		
	}
}

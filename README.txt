Herramientas y datos usados

[Java]
Twitter4j: Biblioteca Java para recuperar tweets georeferenciados.
GeoTools: Biblioteca Java para interpretar Shape Files (Mapa de comunas de la CABA)
	*gt-epsg-hsql: Tiene el Coordinate Reference System para convertir [x,y] a [Lat,Lng]
Hibernate: ORM

====BBDD====
PostgreSQL: Persistencia de tweets.


[Visualizacion]
CartoDB: Motor de visualización online para CSVs geolocalizados.
https://tandil.cartodb.com/tables/twittermonitor/public/map#/visualize

[Datos]
Mapa comunas: http://www.buenosaires.gob.ar/areas/hacienda/sis_estadistico/cartografia_censal_cnphv_2010.php?menu_id=35240